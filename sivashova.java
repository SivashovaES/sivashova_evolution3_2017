package seabattle.model.gameobjects;


/**
 * Островок суши - игровой объект, находящийся на поле.
 *
 * Может быть взорван вследствие поражения всех своих клеток.
 */
public class Island extends FieldObject {
    
    /**
     * Конструктор класса.
     *
     * Инициализирует  поля класса.
     *
     * @param field ссылка на игровое поле
     */
    public Island(GameField field) {
        super(field);
    }
    
    // -------------------------- Выстрел по клетке --------------------------
    
   
    
    // -------------------------- Уничтожение --------------------------
    
    /**
     * Уничтожиться
     */
    private void destroy() {
        
        // Открыть соседние клетки в радиусе 1
        _field.openNeighbouringCells(this, 1);    
    }
    
    // ------------------------ События и слушатели -------------------------
  
    // Список слушателей
    private ArrayList _listenerList = new ArrayList(); 
 
    /**
     * Присоединяет слушателя
     * 
     * @param l слушатель
     */
    public void addGameListener(ResultOfShotListener l) { 
        _listenerList.add(l); 
    }
    
    /**
     * Отсоединяет слушателя
     * 
     * @param l слушатель
     */
    public void removeGameListener(ResultOfShotListener l) { 
        _listenerList.remove(l); 
    } 
    
    /**
     * Оповещает слушателей о успешности последнего хода
     * 
     * @param success признак успешности
     */
    private void fireSetSuccessOfLastShot(boolean success) {
        
        ResultOfShotEvent event = new ResultOfShotEvent(this);
        for (Object listner : _listenerList)
        {
            ((ResultOfShotListener)listner).successfullLastShot(event, success);
        }
    } 
}
